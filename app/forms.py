from django.forms import ModelForm
from models import Paciente, Pais, Enfermero, Diresa, Red, Microred, Centrosalud, TipoPaciente, Vacuna


class PacienteForm(ModelForm):
    class Meta:
        model = Paciente

class TipoPacienteForm(ModelForm):
    class Meta:
        model = TipoPaciente

class PaisForm(ModelForm):
    class Meta:
        model = Pais

class EnfermeroForm(ModelForm):
    class Meta:
        model = Enfermero

class CentrosaludForm(ModelForm):
    class Meta:
        model = Centrosalud

class RedForm(ModelForm):
    class Meta:
        model = Red

class DiresaForm(ModelForm):
    class Meta:
        model = Diresa

class MicroredForm(ModelForm):
    class Meta:
        model = Microred

class VacunaForm(ModelForm):
    class Meta:
        model = Vacuna