from django.db import models

# Create your models here.

#------ tablas maestras
class Diresa(models.Model):
    nombre = models.CharField(max_length=120)
    class Meta: db_table = 'Diresa'

    def __unicode__(self):
        return self.nombre

class TipoPaciente(models.Model):
    descripcion = models.CharField(max_length=50)
    class Meta: db_table = 'TipoPaciente'

    def __unicode__(self):
        return self.descripcion

class Distrito(models.Model):
    nombre = models.CharField(max_length=60)
    class Meta: db_table = 'Distrito'

    def __unicode__(self):
        return self.nombre

class Pais(models.Model):
    nombre = models.CharField(max_length=50)
    class Meta: db_table = 'Pais'

    def __unicode__(self):
        return self.nombre

class Nivel(models.Model):
    descripcion = models.TextField()
    class Meta: db_table = 'Nivel'

    def __unicode__(self):
        return u'%s' % self.descripcion





#-------------------------------------------------------

class Red(models.Model):
    diresa = models.ForeignKey(Diresa)
    nombre = models.CharField(max_length=120)
    class Meta: db_table = 'Red'

    def __unicode__(self):
        return u'%s' % self.nombre

class Microred(models.Model):
    red = models.ForeignKey(Red)
    nombre = models.CharField(max_length=120)
    class Meta: db_table = 'Microred'

    def __unicode__(self):
        return u'%s' % self.nombre

class Centrosalud(models.Model):
    microred = models.ForeignKey(Microred)
    nivel = models.ForeignKey(Nivel)
    distrito = models.ForeignKey(Distrito)
    nombre = models.CharField(max_length=120)
    direccion = models.CharField(max_length=250)
    class Meta: db_table = 'Centrosalud'

    def __unicode__(self):
        return u'%s' % self.nombre

class Enfermero(models.Model):
    distrito = models.ForeignKey(Distrito)
    centrosalud = models.ForeignKey(Centrosalud)
    nombre = models.CharField(max_length=150)
    apePat = models.CharField(max_length=30)
    apeMat = models.CharField(max_length=30)
    fec_nacimiento = models.DateField()
    direccion = models.CharField(max_length=250)
    sexo = models.CharField(max_length=1)
    telefono = models.CharField(max_length=15)
    celular = models.CharField(max_length=9)
    class Meta: db_table = 'Enfermero'

    def __unicode__(self):
        return self.nombre

class Vacuna(models.Model):
    nombre_vac = models.CharField(max_length=180)
    fecha_exp = models.DateField()
    lote = models.IntegerField(max_length=60)
    class Meta: db_table = 'Vacuna'

    def __unicode__(self):
        return u'%s' % self.nombre_vac

class Ingreso(models.Model):
    centrosalud = models.ForeignKey(Centrosalud)
    vacuna = models.OneToOneField(Vacuna)
    fecha_ing = models.DateField()
    cantidad = models.IntegerField()
    class Meta: db_table = 'Ingreso'

    def __unicode__(self):
        return u'%s' % self.fecha_ing

class Paciente(models.Model):
    dni = models.IntegerField(primary_key=True)
    tipopaciente= models.ForeignKey(TipoPaciente)
    pais = models.ForeignKey(Pais)
    fec_nacimiento = models.DateField()
    nombres = models.CharField(max_length=150)
    apePat = models.CharField(max_length=30)
    apeMat = models.CharField(max_length=30)
    direccion = models.CharField(max_length=250)
    sexo = models.CharField(max_length=1)
    profesion = models.CharField(max_length=120)
    telefono = models.CharField(max_length=15)
    celular = models.CharField(max_length=9)
    distrito = models.ForeignKey(Distrito)

    class Meta: db_table = 'Paciente'

    def __unicode__(self):
        return u'%s' % self.nombres

class Historia_clinica(models.Model):
    paciente = models.OneToOneField(Paciente)
    descripcion = models.CharField(max_length=80)
    class Meta: db_table = 'Historia_clinica'

    def __unicode__(self):
        return u'%s' % self.paciente

class Aplicacion_vacuna(models.Model):
    vacuna = models.ForeignKey(Vacuna)
    enfermero = models.ForeignKey(Enfermero)
    historia_clinica = models.ForeignKey(Historia_clinica)
    fech_aplic = models.DateField()
    class Meta: db_table = 'Aplicacion_vacuna'

    def __unicode__(self):
        return u'%s' % self.vacuna

class Programacion(models.Model):
    centrosalud = models.ForeignKey(Centrosalud)
    vacuna = models.OneToOneField(Vacuna)
    fecha_ini = models.DateField()
    fecha_fin = models.DateField()
    cantidad = models.IntegerField()
    class Meta: db_table = 'Programacion'

    def __unicode__(self):
        return u'%s' % self.fecha_ini













