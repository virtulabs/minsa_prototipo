from django.shortcuts import render
from django.shortcuts import render_to_response
from django.views.generic import TemplateView, ListView, FormView, CreateView, UpdateView
from models import Paciente, TipoPaciente, Pais, Enfermero, Centrosalud, Diresa, Red, Microred, Vacuna
from forms import PacienteForm, TipoPacienteForm, PaisForm, EnfermeroForm, CentrosaludForm, DiresaForm, RedForm, MicroredForm, VacunaForm

from django.template import RequestContext




class Home(TemplateView):
    template_name ='mostrar.html'



#============ Paciente =============
class ListarPaciente(TemplateView):
    template_name = 'listar_paciente.html'

    def get_context_data(self, **kwargs):
        context = super(ListarPaciente, self).get_context_data(**kwargs)
        context['lista_data'] = Paciente.objects.all()
        return context


class AgregarPaciente(CreateView):
    template_name = 'agregar.html'
    model = Paciente
    form_class = PacienteForm
    success_url = '/'

class EditarPaciente(UpdateView):
    template_name = 'agregar.html'
    model = Paciente
    from_class = PacienteForm
    success_url = '/'

    def get_object(self, queryset=None):
        obj = Paciente.objects.get(pk=self.kwargs['pk'])
        return obj

#==========tipo de paciente===========
class ListarTipoPaciente(TemplateView):
    template_name = 'listar_tipo_paciente.html'

    def get_context_data(self, **kwargs):
        context = super(ListarTipoPaciente, self).get_context_data(**kwargs)
        context['lista_data'] = TipoPaciente.objects.all()
        return context

class AgregarTipoPaciente(CreateView):
    template_name = 'agregartipo.html'
    model = TipoPaciente
    from_class = TipoPacienteForm
    success_url = '/'

class EditarTipoPaciente(UpdateView):
    template_name = 'agregartipo.html'
    model = TipoPaciente
    form_class = TipoPacienteForm
    success_url = '/'

    def get_object(self, queryset=None):
        obj = TipoPaciente.objects.get(pk=self.kwargs['pk'])
        return obj


#=============Pais==================

class ListarPais(TemplateView):
    template_name = 'listar_pais.html'

    def get_context_data(self, **kwargs):
        context = super(ListarPais, self).get_context_data(**kwargs)
        context['lista_data'] = Pais.objects.all()
        return context


class AgregarPais(CreateView):
    template_name = 'agregarpais.html'
    model = Pais
    from_class = PaisForm
    success_url = '/'

class EditarPais(UpdateView):
    template_name = 'agregarpais.html'
    model = Pais
    form_class = PaisForm
    success_url = '/'

    def get_object(self, queryset=None):
        obj = Pais.objects.get(pk=self.kwargs['pk'])
        return obj



#===========Enfermero===========
class ListarEnfermero(TemplateView):
    template_name = 'listar_enfermero.html'

    def get_context_data(self, **kwargs):
        context = super(ListarEnfermero, self).get_context_data(**kwargs)
        context['lista_data'] = Enfermero.objects.all()
        return context


class AgregarEnfermero(CreateView):
    template_name = 'agregarenfermero.html'
    model = Enfermero
    from_class = EnfermeroForm
    success_url = '/'


class EditarEnfermero(UpdateView):
    template_name = 'agregarenfermero.html'
    model = Enfermero
    form_class = EnfermeroForm
    success_url = '/'

    def get_object(self, queryset=None):
        obj = Enfermero.objects.get(pk=self.kwargs['pk'])
        return obj


#================Centro de salud =========================

class ListarCentrosalud(TemplateView):
    template_name = 'listar_centro_salud.html'

    def get_context_data(self, **kwargs):
        context = super(ListarCentrosalud, self).get_context_data(**kwargs)
        context['lista_data'] = Centrosalud.objects.all()
        return context

class AgregarCentrosalud(CreateView):
    template_name = 'agregarcs.html'
    model = Centrosalud
    form_class = CentrosaludForm
    success_url = '/'


class EditarCentrosalud(UpdateView):
    template_name = 'agregarcs.html'
    model = Centrosalud
    form_class = CentrosaludForm
    success_url = '/'

    def get_object(self, queryset=None):
        obj = Centrosalud.objects.get(pk=self.kwargs['pk'])
        return obj


#===================Micro red =================================
class ListarMicrored(TemplateView):
    template_name = 'listar_micro_red.html'

    def get_context_data(self, **kwargs):
        context = super(ListarMicrored, self).get_context_data(**kwargs)
        context['lista_data'] = Microred.objects.all()
        return context

class AgregarMicrored(CreateView):
    template_name = 'agregarmicro.html'
    model = Microred
    form_class = MicroredForm
    success_url = '/'



class EditarMicrored(UpdateView):
    template_name = 'agregarmicro.html'
    model = Microred
    form_class = MicroredForm
    success_url = '/'

    def get_object(self, queryset=None):
        obj = Microred.objects.get(pk=self.kwargs['pk'])
        return obj



#=====================RED ===========================
class ListarRed(TemplateView):
    template_name = 'listar_red.html'

    def get_context_data(self, **kwargs):
        context = super(ListarRed, self).get_context_data(**kwargs)
        context['lista_data'] = Red.objects.all()
        return context


class AgregarRed(CreateView):
    template_name = 'agregarred.html'
    model = Red
    form_class = RedForm
    success_url = '/'


class EditarRed(UpdateView):
    template_name = 'agregarred.html'
    model = Red
    form_class = RedForm
    success_url = '/'

    def get_object(self, queryset=None):
        obj = Red.objects.get(pk=self.kwargs['pk'])
        return obj

#=====================DIRESA=================================
class ListarDiresa(TemplateView):
    template_name = 'listar_diresa.html'

    def get_context_data(self, **kwargs):
        context = super(ListarDiresa, self).get_context_data(**kwargs)
        context['lista_data'] = Diresa.objects.all()
        return context


class AgregarDiresa(CreateView):
    template_name =  'agregardiresa.html'
    model = Diresa
    form_class = DiresaForm
    success_url = '/'


class EditarDiresa(UpdateView):
    template_name = 'agregardiresa.html'
    model = Diresa
    form_class = DiresaForm
    success_url = '/'

    def get_object(self, queryset=None):
        obj = Diresa.objects.get(pk=self.kwargs['pk'])
        return obj


#=====================Vacuna=======================
class ListarVacuna(TemplateView):
    template_name = 'listar_vacuna.html'

    def get_context_data(self, **kwargs):
        context = super(ListarVacuna, self).get_context_data(**kwargs)
        context['lista_data'] = Vacuna.objects.all()
        return context


class AgregarVacuna(CreateView):
    template_name = 'agregarvacuna.html'
    model = Vacuna
    form_class = VacunaForm
    success_url = '/'


class EditarVacuna(UpdateView):
    template_name = 'agregarvacuna.html'
    model = Vacuna
    form_class = VacunaForm
    success_url = '/'

    def get_context_data(self, **kwargs):
        obj = Vacuna.objects.get(pk=self.kwargs['pk'])
        return obj

















