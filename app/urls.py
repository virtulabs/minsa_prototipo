from django.conf.urls import patterns, include, url

import views

urlpatterns = patterns('',
    url(r'^$', views.Home.as_view(), name='home'),

    url(r'^listarpaciente/?$', views.ListarPaciente.as_view(), name='listar-paciente' ),
    url(r'^agregar/?$', views.AgregarPaciente.as_view(), name='agregar'),
    url(r'^editarpaciente/(?P<pk>\d+)/?$', views.EditarPaciente.as_view(), name='editarpaciente'),

    url(r'^listartipo/$', views.ListarTipoPaciente.as_view(), name='listar-tipo'),
    url(r'^agregartipo/?$', views.AgregarTipoPaciente.as_view(), name='agregartipo'),
    url(r'^editartipo/(?P<pk>\d+)/?$', views.EditarTipoPaciente.as_view(), name='editartipo'),

    url(r'^listarpais/$', views.ListarPais.as_view(), name='listar-pais'),
    url(r'^agregarpais/?$', views.AgregarPais.as_view(), name='agregarpais'),
    url(r'^editarpais/(?P<pk>\d+)/?$', views.EditarPais.as_view(), name='editarpais'),

    url(r'^listarenfermero/?$', views.ListarEnfermero.as_view(), name='listar-enfermero'),
    url(r'^agregarenfermero/?$', views.AgregarEnfermero.as_view(), name='agregarenfermero'),
    url(r'^editarenfermero/(?P<pk>\d+)/?$', views.EditarEnfermero.as_view(), name='editarenfermero'),

    url(r'^listarcs/?$', views.ListarCentrosalud.as_view(), name='listar-cs'),
    url(r'^agregarcs/?$', views.AgregarCentrosalud.as_view(), name='agregarcs'),
    url(r'^editarcs/(?P<pk>\d+)/?$', views.EditarCentrosalud.as_view(), name='editarcs'),


    url(r'^listarmicro/?$', views.ListarMicrored.as_view(), name='listar-micro'),
    url(r'^agregarmicro/?$', views.AgregarMicrored.as_view(), name='agregarmicro'),
    url(r'^editarmicro/(?P<pk>\d+)/?$', views.EditarMicrored.as_view(), name='editarmicro'),

    url(r'^listarred/?$', views.ListarRed.as_view(), name='listar-red'),
    url(r'^agregarred/?$', views.AgregarRed.as_view(), name='agregarred'),
    url(r'^editarred/(?P<pk>\d+)/?$', views.EditarRed.as_view(), name='editarred'),

    url(r'^listardiresa/?$', views.ListarDiresa.as_view(), name='listar-diresa'),
    url(r'^agregardiresa/?$', views.AgregarDiresa.as_view(), name='agregardiresa'),
    url(r'^editardiresa/(?P<pk>\d+)/?$', views.EditarDiresa.as_view(), name='editardiresa'),

    url(r'^listarvacuna/?$', views.ListarVacuna.as_view(), name='listar-vacuna'),
    url(r'^agregarvacuna/?$', views.AgregarVacuna.as_view(), name='agregarvacuna'),
    url(r'^editarvacuna/(?P<pk>\d+)/?$', views.EditarVacuna.as_view(), name='editarvacuna'),


)