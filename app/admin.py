from django.contrib import admin
from app.models import Diresa,TipoPaciente, Pais, Nivel, Enfermero, Red, Microred, Centrosalud, Vacuna, Ingreso, Paciente, Aplicacion_vacuna, Historia_clinica, Programacion

# Register your models here.

admin.site.register(Diresa)
admin.site.register(TipoPaciente)
admin.site.register(Pais)
admin.site.register(Nivel)
admin.site.register(Enfermero)
admin.site.register(Red)
admin.site.register(Microred)
admin.site.register(Centrosalud)
admin.site.register(Vacuna)
admin.site.register(Ingreso)
admin.site.register(Paciente)
admin.site.register(Aplicacion_vacuna)
admin.site.register(Historia_clinica)
admin.site.register(Programacion)